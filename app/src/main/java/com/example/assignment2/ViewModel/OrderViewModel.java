package com.example.assignment2.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.assignment2.database.OrderRepository;
import com.example.assignment2.model.Order;

import java.util.List;

public class OrderViewModel extends AndroidViewModel {

    private static final String TAG = "OrderViewModel";

    private OrderRepository orderRepository;
    private LiveData<List<Order>> orders;

    public OrderViewModel(@NonNull Application application) {
        super(application);
        orderRepository = new OrderRepository(application);
        orders = orderRepository.getAllOrders();
    }

    public int countOrdersForNextOrdersId(){
        LiveData<List<Order>> orders = orderRepository.getAllOrders();
        List<Order> orderList = orders.getValue();
        //searching for order with biggest id
        if (orderList == null) return 0;
        int returner = orderList.get(0).getId();
        for (Order o: orderList) {
            if (o.getId() > returner) returner = o.getId();
        }
        return returner;
    }

    public LiveData<List<Order>> getOrders() {
        return orders;
    }

    public void insert(Order order) {
        orderRepository.insertOrder(order);
    }

    public LiveData<List<Order>> findOrdersByCustomerId(String id) {
        return orderRepository.getOrdersByCustomerId(id);
    }
}
