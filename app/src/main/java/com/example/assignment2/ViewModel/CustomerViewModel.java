package com.example.assignment2.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.assignment2.database.CustomerRepository;
import com.example.assignment2.model.Customer;

import java.util.List;

public class CustomerViewModel extends AndroidViewModel {

    private static final String TAG = "CustomerViewModel";

    private CustomerRepository customerRepository;
    private LiveData<List<Customer>> customers;

    public CustomerViewModel(@NonNull Application application) {
        super(application);
        customerRepository = new CustomerRepository(application);
        customers = customerRepository.getAllCustomers();
        Log.d(TAG, " created.");
    }

    public LiveData<List<Customer>> getCustomers() {
        return customers;
    }

    public Customer getCustomerByEmail(String email){
        Log.d(TAG, " getCustomerByEmail.");
        Customer customer = new Customer();
        customer.setId("");
        customer.setPassword("");
        customer.setEmail(email);
        customer.setName("");
        return customerRepository.getCustomerByEmail(customer);
    }

    public void insert(Customer customer) {
        Log.d(TAG, " inserted customer.");
        customerRepository.insertCustomer(customer); }
}
