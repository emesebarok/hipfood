package com.example.assignment2.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.assignment2.database.MenuRepository;
import com.example.assignment2.model.Menu;

import java.util.List;

public class MenuViewModel extends AndroidViewModel {

    private MenuRepository menuRepository;
    private LiveData<List<Menu>> menus;

    public MenuViewModel(@NonNull Application application) {
        super(application);
        menuRepository = new MenuRepository(application);
        menus = menuRepository.loadAllMenus();
    }

    public LiveData<List<Menu>> getMenus() {
        return menus;
    }

    public LiveData<List<Menu>> getMenus(int id) {
        return menuRepository.getAllMenus(id);
    }
}
