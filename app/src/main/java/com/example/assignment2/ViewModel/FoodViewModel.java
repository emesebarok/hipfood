package com.example.assignment2.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.assignment2.database.FoodRepository;
import com.example.assignment2.model.Food;

import java.util.List;

public class FoodViewModel extends AndroidViewModel {

    private static final String TAG = "FoodViewModel";

    private FoodRepository foodRepository;
    private LiveData<List<Food>> foods;

    public FoodViewModel(@NonNull Application application) {
        super(application);
        foodRepository = new FoodRepository(application);
        foods = foodRepository.loadAllFoods();
        Log.d(TAG, " created.");
    }

    public LiveData<List<Food>> getFoods() {
        return foods;
    }

    //by menuid
    public LiveData<List<Food>> getAllFoods(int id) {
        return foodRepository.getAllFoods(id);
    }

    public Food getFood(int id) {
        Log.d(TAG, "getFood");
        //return foodRepository.getFood(id);
        Food food = new Food();
        food.setName("");
        food.setMenuID(0);
        food.setId(id);
        return foodRepository.getFood(food);
    }

    public int getRestId(int id) {
        return foodRepository.getRestId(id);
    }
}
