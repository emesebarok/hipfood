package com.example.assignment2.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.assignment2.database.RestaurantRepository;
import com.example.assignment2.model.Restaurant;

import java.util.List;

public class RestaurantViewModel extends AndroidViewModel {

    private LiveData<List<Restaurant>> restaurants;

    private RestaurantRepository restaurantRepository;

    public RestaurantViewModel(@NonNull Application application) {
        super(application);
        restaurantRepository = new RestaurantRepository(application);
        restaurants = restaurantRepository.getAllRestaurants();
    }

    public LiveData<List<Restaurant>> getRestaurants() {
        return restaurants;
    }

    public Restaurant getRestaurantById(int id) {return restaurantRepository.getRestaurantById(id);}
}
