package com.example.assignment2.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.assignment2.database.OrderDetailsRepository;
import com.example.assignment2.model.OrderDetails;

import java.util.List;
import java.util.ListIterator;

public class OrderDetailsViewModel extends AndroidViewModel {

    private static final String TAG = "OrderViewModel";

    private OrderDetailsRepository orderDetailsRepository;
    private LiveData<List<OrderDetails>> orderDetails;

    public OrderDetailsViewModel(@NonNull Application application) {
        super(application);
        orderDetailsRepository = new OrderDetailsRepository(application);
        orderDetails = orderDetailsRepository.getAllOrderDetails();
    }

    public int countOrdersForNextOrderDetailsId(){
        LiveData<List<OrderDetails>> orderDetails = orderDetailsRepository.getAllOrderDetails();
        List<OrderDetails> orderList = orderDetails.getValue();
        //searching for order with biggest id
        if (orderList == null) return 0;
        int returner = orderList.get(0).getId();
        for (OrderDetails o: orderList) {
            if (o.getId() > returner) returner = o.getId();
        }
        return returner;
    }

    public LiveData<List<OrderDetails>> getOrderDetails() {
        return orderDetails;
    }

    public LiveData<List<OrderDetails>> getOrderDetailsByOrderId(int id) {
        return orderDetailsRepository.getOrderDetailByOrderId(id);
    }

    public void insert (OrderDetails o) {
        orderDetailsRepository.insertOrderDetail(o);
    }
}
