package com.example.assignment2.database;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.assignment2.DAO.MenuDAO;
import com.example.assignment2.DAO.OrderDetailsDAO;
import com.example.assignment2.model.Menu;
import com.example.assignment2.model.OrderDetails;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class MenuRepository {

    private MenuDAO menuDAO;
    private static LiveData<List<Menu>> menus;

    public MenuRepository(Application application) {
        AppRoomDatabase db;
        db = AppRoomDatabase.getDatabase(application);
        menuDAO = db.menuDAO();
    }

    public void insertMenu(Menu menu) {
        new MenuRepository.InsertAsyncTask(menuDAO).execute(menu);
    }

    public void deleteMenu(Menu menu) {
        new MenuRepository.DeleteAsyncTask(menuDAO).execute(menu);
    }

    public LiveData<List<Menu>> loadAllMenus() {
        try {
            menus = new MenuRepository.SelectAllAsyncTask(menuDAO).execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return menus;
    }

    public LiveData<List<Menu>> getAllMenus(int id) {
        return menuDAO.loadAllMenusByRestId(id);
    }

    private static void asyncFinishedAll(LiveData<List<Menu>> m) {
        menus = m;
    }

    private static class SelectAllAsyncTask extends AsyncTask<Void, Void, LiveData<List<Menu>>> {

        private MenuDAO asyncTaskDao;

        SelectAllAsyncTask(MenuDAO dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected LiveData<List<Menu>> doInBackground(final Void... params) {
            return asyncTaskDao.loadAllMenus();
        }

        @Override
        protected  void onPostExecute (LiveData<List<Menu>> m) {
            MenuRepository.asyncFinishedAll(m);
        }
    }

    private static class InsertAsyncTask extends AsyncTask<Menu, Void, Void> {

        private MenuDAO asyncTaskDao;

        InsertAsyncTask(MenuDAO dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Menu... params) {
            asyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class DeleteAsyncTask extends AsyncTask<Menu, Void, Void> {

        private MenuDAO asyncTaskDao;

        DeleteAsyncTask(MenuDAO dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Menu... params) {
            asyncTaskDao.delete(params[0]);
            return null;
        }
    }
}
