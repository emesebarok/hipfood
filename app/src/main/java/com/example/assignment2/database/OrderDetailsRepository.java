package com.example.assignment2.database;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.assignment2.DAO.OrderDetailsDAO;
import com.example.assignment2.model.OrderDetails;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class OrderDetailsRepository {

    private OrderDetailsDAO orderDetailsDAO;
    private static LiveData<List<OrderDetails>> searchResult;
    private static LiveData<List<OrderDetails>> orderDetails;

    public OrderDetailsRepository(Application application) {
        AppRoomDatabase db;
        db = AppRoomDatabase.getDatabase(application);
        orderDetailsDAO = db.orderDetailsDAO();
    }

    public void insertOrderDetail(OrderDetails orderDetails) {
        new OrderDetailsRepository.InsertAsyncTask(orderDetailsDAO).execute(orderDetails);
    }

    public void updateOrderDetail(OrderDetails orderDetails) {
        new OrderDetailsRepository.UpdateAsyncTask(orderDetailsDAO).execute(orderDetails);
    }

    public void deleteOrderDetail(OrderDetails orderDetails) {
        new OrderDetailsRepository.DeleteAsyncTask(orderDetailsDAO).execute(orderDetails);
    }

    public LiveData<List<OrderDetails>> getAllOrderDetails() {
        try {
            orderDetails = new OrderDetailsRepository.SelectAllAsyncTask(orderDetailsDAO).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return orderDetails;
    }

    public LiveData<List<OrderDetails>> getOrderDetailByOrderId (Integer id) {
        try {
            searchResult = new OrderDetailsRepository.SelectAsyncTask(orderDetailsDAO).execute(id).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return searchResult;
    }

    private static void asyncFinishedAll(LiveData<List<OrderDetails>> od) {
        orderDetails = od;
    }

    private static class SelectAllAsyncTask extends AsyncTask<Void, Void, LiveData<List<OrderDetails>>> {

        private OrderDetailsDAO asyncTaskDao;

        SelectAllAsyncTask(OrderDetailsDAO dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected LiveData<List<OrderDetails>> doInBackground(final Void... params) {
            return asyncTaskDao.loadAllOrderDetails();
        }

        @Override
        protected  void onPostExecute (LiveData<List<OrderDetails>> orderDetails) {
            OrderDetailsRepository.asyncFinishedAll(orderDetails);
        }
    }

    private static void asyncFinished(LiveData<List<OrderDetails>> orderDetails) {
        searchResult = orderDetails;
    }

    private static class SelectAsyncTask extends AsyncTask<Integer, Void, LiveData<List<OrderDetails>>> {

        private OrderDetailsDAO asyncTaskDao;

        SelectAsyncTask(OrderDetailsDAO dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected LiveData<List<OrderDetails>> doInBackground(final Integer... params) {
            return asyncTaskDao.loadOrderDetailsById(params[0]);
        }

        @Override
        protected  void onPostExecute (LiveData<List<OrderDetails>> orderDetails) {
            OrderDetailsRepository.asyncFinished(orderDetails);
        }
    }

    private static class InsertAsyncTask extends AsyncTask<OrderDetails, Void, Void> {

        private OrderDetailsDAO asyncTaskDao;

        InsertAsyncTask(OrderDetailsDAO dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final OrderDetails... params) {
            asyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class UpdateAsyncTask extends AsyncTask<OrderDetails, Void, Void> {

        private OrderDetailsDAO asyncTaskDao;

        UpdateAsyncTask(OrderDetailsDAO dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final OrderDetails... params) {
            asyncTaskDao.update(params[0]);
            return null;
        }
    }

    private static class DeleteAsyncTask extends AsyncTask<OrderDetails, Void, Void> {

        private OrderDetailsDAO asyncTaskDao;

        DeleteAsyncTask(OrderDetailsDAO dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final OrderDetails... params) {
            asyncTaskDao.delete(params[0]);
            return null;
        }
    }
}
