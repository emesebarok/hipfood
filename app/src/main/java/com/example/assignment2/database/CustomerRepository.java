package com.example.assignment2.database;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.assignment2.DAO.CustomerDAO;
import com.example.assignment2.model.Customer;

import java.util.List;

public class CustomerRepository {

    private static Customer searchResults;

    private CustomerDAO customerDAO;
    private static LiveData<List<Customer>> customers;

    public CustomerRepository(Application application) {
        AppRoomDatabase db;
        db = AppRoomDatabase.getDatabase(application);
        customerDAO = db.customerDAO();
    }

    public void insertCustomer(Customer customer) {
        new InsertAsyncTask(customerDAO).execute(customer);
    }

    public void deleteCustomer(Customer customer) {
        new DeleteAsyncTask(customerDAO).execute(customer);
    }

    public LiveData<List<Customer>> getAllCustomers() {
        return customerDAO.allCustomers();
    }

    public Customer getCustomerByEmail(Customer customer) {
        new SelectByMailAsyncTask(customerDAO).execute(customer);
        return searchResults;
    }

    private static void asyncFinishedAll(LiveData<List<Customer>> results) {
        customers = results;
    }

    private static class SelectAllAsyncTask extends AsyncTask<Void, Void, LiveData<List<Customer>>> {
        private CustomerDAO asyncTaskDao;

        SelectAllAsyncTask(CustomerDAO dao) {asyncTaskDao=dao;}

        @Override
        protected LiveData<List<Customer>> doInBackground(final Void... params) {
            return asyncTaskDao.allCustomers();
        }

        @Override
        protected void onPostExecute(LiveData<List<Customer>> result) {
            CustomerRepository.asyncFinishedAll(result);
        }
    }

    private static void asyncMailFinished(Customer results) {
        searchResults = results;
    }

    private static class SelectByMailAsyncTask extends AsyncTask<Customer, Void, Customer> {
        private CustomerDAO asyncTaskDao;

        SelectByMailAsyncTask(CustomerDAO dao) {asyncTaskDao=dao;}

        @Override
        protected Customer doInBackground(final Customer... params) {
            return asyncTaskDao.findCustomerByEmail(params[0].getEmail());
        }

        @Override
        protected void onPostExecute(Customer result) {
            CustomerRepository.asyncMailFinished(result);
        }
    }

    private static class InsertAsyncTask extends AsyncTask<Customer, Void, Void> {

        private CustomerDAO asyncTaskDao;

        InsertAsyncTask(CustomerDAO dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Customer... params) {
            asyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class DeleteAsyncTask extends AsyncTask<Customer, Void, Void> {

        private CustomerDAO asyncTaskDao;

        DeleteAsyncTask(CustomerDAO dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Customer... params) {
            asyncTaskDao.delete(params[0]);
            return null;
        }
    }
}
