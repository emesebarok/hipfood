package com.example.assignment2.database;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.assignment2.DAO.CustomerDAO;
import com.example.assignment2.DAO.OrderDAO;
import com.example.assignment2.model.Order;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class OrderRepository {

    private OrderDAO orderDAO;
    private static LiveData<List<Order>> searchedOrder;
    private static LiveData<List<Order>> orders;

    public OrderRepository(Application application) {
        AppRoomDatabase db;
        db = AppRoomDatabase.getDatabase(application);
        orderDAO = db.orderDAO();
    }

    public void insertOrder(Order order) {
        new OrderRepository.InsertAsyncTask(orderDAO).execute(order);
    }

    public void updateOrder(Order order) {
        new OrderRepository.UpdateAsyncTask(orderDAO).execute(order);
    }

    public void deleteOrder(Order order) {
        new OrderRepository.DeleteAsyncTask(orderDAO).execute(order);
    }

    public LiveData<List<Order>> getAllOrdersByRestId(int id) {
        return orderDAO.loadAllOrdersByRestId(id);
    }

    public LiveData<List<Order>> getOrdersByCustomerId(String id) {
        try {
            searchedOrder = new OrderRepository.SelectAsyncTask(orderDAO).execute(id).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return searchedOrder;
    }

    public LiveData<List<Order>> getAllOrders() {
        try {
            orders = new OrderRepository.SelectAllAsyncTask(orderDAO).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return orders;
    }

    private static void asyncFinishedAll(LiveData<List<Order>> results) { orders = results; }

    private static class SelectAllAsyncTask extends AsyncTask<Void, Void, LiveData<List<Order>>> {
        private OrderDAO asyncTaskDao;

        SelectAllAsyncTask(OrderDAO dao) {asyncTaskDao=dao;}

        @Override
        protected LiveData<List<Order>> doInBackground(final Void... params) {
            return asyncTaskDao.loadAllOrders();
        }

        @Override
        protected void onPostExecute(LiveData<List<Order>> result) {
            OrderRepository.asyncFinishedAll(result);
        }
    }

    private static void asyncFinished(LiveData<List<Order>> results) { searchedOrder = results; }

    private static class SelectAsyncTask extends AsyncTask<String, Void, LiveData<List<Order>>> {
        private OrderDAO asyncTaskDao;

        SelectAsyncTask(OrderDAO dao) {asyncTaskDao=dao;}

        @Override
        protected LiveData<List<Order>> doInBackground(final String... params) {
            return asyncTaskDao.loadOrdersByCustId(params[0]);
        }

        @Override
        protected void onPostExecute(LiveData<List<Order>> result) {
            OrderRepository.asyncFinished(result);
        }
    }

    private static class InsertAsyncTask extends AsyncTask<Order, Void, Void> {

        private OrderDAO asyncTaskDao;

        InsertAsyncTask(OrderDAO dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Order... params) {
            asyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class UpdateAsyncTask extends AsyncTask<Order, Void, Void> {

        private OrderDAO asyncTaskDao;

        UpdateAsyncTask(OrderDAO dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Order... params) {
            asyncTaskDao.update(params[0]);
            return null;
        }
    }

    private static class DeleteAsyncTask extends AsyncTask<Order, Void, Void> {

        private OrderDAO asyncTaskDao;

        DeleteAsyncTask(OrderDAO dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Order... params) {
            asyncTaskDao.delete(params[0]);
            return null;
        }
    }
}
