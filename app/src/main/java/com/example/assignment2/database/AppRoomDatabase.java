package com.example.assignment2.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.assignment2.DAO.CustomerDAO;
import com.example.assignment2.DAO.FoodDAO;
import com.example.assignment2.DAO.MenuDAO;
import com.example.assignment2.DAO.OrderDAO;
import com.example.assignment2.DAO.OrderDetailsDAO;
import com.example.assignment2.DAO.RestaurantDAO;
import com.example.assignment2.model.Customer;
import com.example.assignment2.model.Food;
import com.example.assignment2.model.Menu;
import com.example.assignment2.model.Order;
import com.example.assignment2.model.OrderDetails;
import com.example.assignment2.model.Restaurant;

import java.util.ArrayList;
import java.util.List;

@Database(entities = {Customer.class, Menu.class, Restaurant.class, Food.class, Order.class, OrderDetails.class}, version = 4)
public abstract class AppRoomDatabase extends RoomDatabase {

    private static AppRoomDatabase INSTANCE;

    public abstract CustomerDAO customerDAO();
    public abstract MenuDAO menuDAO();
    public abstract RestaurantDAO restaurantDAO();
    public abstract OrderDAO orderDAO();
    public abstract FoodDAO foodDAO();
    public abstract OrderDetailsDAO orderDetailsDAO();

    static AppRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppRoomDatabase.class, "hip_database")
                            .fallbackToDestructiveMigration()
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final CustomerDAO customerDAO;
        private final RestaurantDAO restaurantDAO;
        private final MenuDAO menuDAO;
        private final FoodDAO foodDAO;

        PopulateDbAsync(AppRoomDatabase db) {

            customerDAO = db.customerDAO();
            restaurantDAO = db.restaurantDAO();
            menuDAO = db.menuDAO();
            foodDAO = db.foodDAO();
        }

        private static Food addFood(final FoodDAO foodDAO, final int id,
                                    final int menuid, final String name) {
            Food food = new Food();
            food.setId(id);
            food.setMenuID(menuid);
            food.setName(name);
            foodDAO.insert(food);
            return food;
        }

        private static Menu addMenu(final MenuDAO menuDAO, final int id,
                                    final int restid, final String name) {
            Menu menu = new Menu();
            menu.setId(id);
            menu.setRestID(restid);
            menu.setName(name);
            menuDAO.insert(menu);
            return menu;
        }

        private static void addRestaurant(final RestaurantDAO restaurantDAO, final int id, final String name) {
            Restaurant restaurant = new Restaurant();
            restaurant.setId(id);
            restaurant.setName(name);
            restaurantDAO.insert(restaurant);
        }

        private static void addCustomer(final CustomerDAO customerDAO, final String id, final String name, final String email, final String password) {
            Customer customer = new Customer();
            customer.setName(name);
            customer.setId(id);
            customer.setEmail(email);
            customer.setPassword(password);
            customerDAO.insert(customer);
        }

        @Override
        protected Void doInBackground(final Void... params) {
            final int DELAY_MILLIS = 500;
            customerDAO.deleteAll();
            foodDAO.deleteAll();
            menuDAO.deleteAll();
            restaurantDAO.deleteAll();

            addCustomer(customerDAO, "Hpci7FjSl1MWwSmGT8yZVhzuXUe2", "Malac", "malac@yahoo.com", "malacpass");
            addCustomer(customerDAO, "Pxt75yfy7nThpw8YJkAsLVVZoCj1", "User", "user@yahoo.com", "userpass");

            addRestaurant(restaurantDAO, 1, "Restaurant1");
            addRestaurant(restaurantDAO, 2, "Restaurant2");
            addRestaurant(restaurantDAO, 3, "Restaurant3");
            try {
                // Loans are added with a delay, to have time for the UI to react to changes.

                //rest1
                List<Food> foods = new ArrayList<>();
                Menu menu = addMenu(menuDAO, 1, 1, "Pasta - menu1rest1");
                foods.add(addFood(foodDAO, 1, 1, "Bolognese - food1menu1rest1"));
                foods.add(addFood(foodDAO, 2, 1, "Carbonara - food2menu1rest1"));
                foods.add(addFood(foodDAO, 3, 1, "Fettuccine Alfredo - food3menu1rest1"));
                menu.setFoodList(foods);
                Thread.sleep(DELAY_MILLIS);

                foods.clear();
                menu = addMenu(menuDAO, 2, 1, "Soup - menu2rest1");
                foods.add(addFood(foodDAO, 4, 2, "Cream - food4menu2rest1"));
                foods.add(addFood(foodDAO, 5, 2, "Ciorba - food5menu2rest1"));
                foods.add(addFood(foodDAO, 6, 2, "Vegetable - food6menu2rest1"));
                menu.setFoodList(foods);
                Thread.sleep(DELAY_MILLIS);

                foods.clear();
                menu = addMenu(menuDAO, 3, 1, "Pizza - menu3rest1");
                foods.add(addFood(foodDAO, 7, 3, "Hawaiian - food7menu3rest1"));
                foods.add(addFood(foodDAO, 8, 3, "Simple - food8menu3rest1"));
                foods.add(addFood(foodDAO, 9, 3, "Spicy - food9menu3rest1"));
                menu.setFoodList(foods);
                Thread.sleep(DELAY_MILLIS);

                //rest2
                foods.clear();
                menu = addMenu(menuDAO, 4, 2, "Pasta - menu1rest2");
                foods.add(addFood(foodDAO, 10, 4, "Bolognese - food10menu1rest2"));
                foods.add(addFood(foodDAO, 11, 4, "Carbonara - food11menu1rest2"));
                foods.add(addFood(foodDAO, 12, 4, "Fettuccine Alfredo - food12menu1rest2"));
                menu.setFoodList(foods);
                Thread.sleep(DELAY_MILLIS);

                foods.clear();
                menu = addMenu(menuDAO, 5, 2, "Soup - menu2rest2");
                foods.add(addFood(foodDAO, 13, 5, "Ciorba - food13menu2rest2"));
                foods.add(addFood(foodDAO, 14, 5, "Cream - food14menu2rest2"));
                menu.setFoodList(foods);
                Thread.sleep(DELAY_MILLIS);

                //rest3
                foods.clear();
                menu = addMenu(menuDAO, 6, 3, "Pizza - menu1rest3");
                foods.add(addFood(foodDAO, 15, 6, "Hawaiian - food15menu1rest3"));
                foods.add(addFood(foodDAO, 16, 6, "Spicy - food16menu1rest3"));
                foods.add(addFood(foodDAO, 17, 6, "Simple - food17menu1rest3"));
                menu.setFoodList(foods);
                Thread.sleep(DELAY_MILLIS);

                foods.clear();
                menu = addMenu(menuDAO, 7, 3, "Pasta - menu2rest3");
                foods.add(addFood(foodDAO, 18, 7, "Bolognese - food18menu2rest3"));
                foods.add(addFood(foodDAO, 19, 7, "Carbonara - food19menu2rest3"));
                menu.setFoodList(foods);
                Thread.sleep(DELAY_MILLIS);

                foods.clear();
                menu = addMenu(menuDAO, 8, 3, "Soup - menu3rest3");
                foods.add(addFood(foodDAO, 20, 8, "Cream - food20menu3rest3"));
                foods.add(addFood(foodDAO, 21, 8, "Vegetable - food21menu3rest3"));
                foods.add(addFood(foodDAO, 22, 8, "Ciorba - food22menu3rest3"));
                menu.setFoodList(foods);

                Log.d("DB", "Added menus and foods");

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }
    }
}
