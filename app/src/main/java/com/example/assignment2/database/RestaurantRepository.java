package com.example.assignment2.database;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.assignment2.DAO.RestaurantDAO;
import com.example.assignment2.model.Restaurant;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class RestaurantRepository {
    private static Restaurant searchedRestaurant;
    private static LiveData<List<Restaurant>> restaurants;

    private RestaurantDAO restaurantDAO;

    public RestaurantRepository(Application application) {
        AppRoomDatabase db;
        db = AppRoomDatabase.getDatabase(application);
        restaurantDAO = db.restaurantDAO();
    }

    public void insertRestaurant(Restaurant restaurant) {
        new RestaurantRepository.InsertAsyncTask(restaurantDAO).execute(restaurant);
    }

    public void deleteRestaurant(Restaurant restaurant) {
        new RestaurantRepository.DeleteAsyncTask(restaurantDAO).execute(restaurant);
    }

    public LiveData<List<Restaurant>> getAllRestaurants() {
        try {
            restaurants = new RestaurantRepository.SelectAllAsyncTask(restaurantDAO).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return restaurants;
    }

    public Restaurant getRestaurantById(Integer id) {
        try {
            searchedRestaurant = new RestaurantRepository.SelectAsyncTask(restaurantDAO).execute(id).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return searchedRestaurant;
    }

    private static void asyncFinishedAll(LiveData<List<Restaurant>> results) { restaurants = results; }

    private static class SelectAllAsyncTask extends AsyncTask<Void, Void, LiveData<List<Restaurant>>> {

        private RestaurantDAO asyncTaskDao;

        SelectAllAsyncTask(RestaurantDAO dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected LiveData<List<Restaurant>> doInBackground(final Void... params) {
            return asyncTaskDao.loadAllRestaurants();
        }

        @Override
        protected void onPostExecute(LiveData<List<Restaurant>> result) {
            RestaurantRepository.asyncFinishedAll(result);
        }
    }

    private static void asyncFinished(Restaurant results) { searchedRestaurant = results; }

    private static class SelectAsyncTask extends AsyncTask<Integer, Void, Restaurant> {

        private RestaurantDAO asyncTaskDao;

        SelectAsyncTask(RestaurantDAO dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Restaurant doInBackground(final Integer... params) {
            return asyncTaskDao.loadRestaurantById(params[0]);
        }

        @Override
        protected void onPostExecute(Restaurant result) {
            RestaurantRepository.asyncFinished(result);
        }
    }

    private static class InsertAsyncTask extends AsyncTask<Restaurant, Void, Void> {

        private RestaurantDAO asyncTaskDao;

        InsertAsyncTask(RestaurantDAO dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Restaurant... params) {
            asyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class DeleteAsyncTask extends AsyncTask<Restaurant, Void, Void> {

        private RestaurantDAO asyncTaskDao;

        DeleteAsyncTask(RestaurantDAO dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Restaurant... params) {
            asyncTaskDao.delete(params[0]);
            return null;
        }
    }
}
