package com.example.assignment2.database;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;

import com.example.assignment2.DAO.FoodDAO;
import com.example.assignment2.model.Food;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class FoodRepository {
    private static final String TAG = "FoodRepository";

    private FoodDAO foodDAO;

    private static Food searchResults;
    private static LiveData<List<Food>> foods;
    private static int sr;

    public FoodRepository(Application application) {
        AppRoomDatabase db;
        db = AppRoomDatabase.getDatabase(application);
        foodDAO = db.foodDAO();
    }

    public void insertFood(Food food) {
        new FoodRepository.InsertAsyncTask(foodDAO).execute(food);
    }

    public void deleteFood(Food food) {
        new FoodRepository.DeleteAsyncTask(foodDAO).execute(food);
    }

    public LiveData<List<Food>> loadAllFoods() {
        try {
            foods = new FoodRepository.SelectAllAsyncTask(foodDAO).execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return foods;
    }

    public LiveData<List<Food>> getAllFoods(int id) {
        return foodDAO.loadAllFoodsByMenuId(id);
    }

    public Food getFood(Food food) {
        try {
            searchResults = new SelectAsyncTask(foodDAO).execute(food).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return searchResults;
    }

    public int getRestId(int foodId) {
        try {
            sr = new SelectRestAsyncTask(foodDAO).execute(foodId).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return sr;
    }

    private static void asyncFinishedAll(LiveData<List<Food>> results) {
        foods = results;
    }

    private static class SelectAllAsyncTask extends AsyncTask<Food, Void, LiveData<List<Food>>> {
        private FoodDAO asyncTaskDao;

        SelectAllAsyncTask(FoodDAO dao) {asyncTaskDao=dao;}

        @Override
        protected LiveData<List<Food>> doInBackground(final Food... params) {
            return asyncTaskDao.loadAllFoods();
        }

        @Override
        protected void onPostExecute(LiveData<List<Food>> result) {
            FoodRepository.asyncFinishedAll(result);
        }
    }

    private static void asyncFinished(Food results) {
        searchResults = results;
    }

    private static class SelectAsyncTask extends AsyncTask<Food, Void, Food> {
        private FoodDAO asyncTaskDao;

        SelectAsyncTask(FoodDAO dao) {asyncTaskDao=dao;}

        @Override
        protected Food doInBackground(final Food... params) {
            return asyncTaskDao.loadFoodById(params[0].getId());
            //return null;
        }

        @Override
        protected void onPostExecute(Food result) {
            FoodRepository.asyncFinished(result);
        }
    }

    private static void asyncRestFinished(Integer results) {
        sr = results;
    }

    private static class SelectRestAsyncTask extends AsyncTask<Integer, Void, Integer> {
        private FoodDAO asyncTaskDao;

        SelectRestAsyncTask(FoodDAO dao) {asyncTaskDao=dao;}

        @Override
        protected Integer doInBackground(final Integer... params) {
            return asyncTaskDao.returnRestId(params[0]);
            //return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            FoodRepository.asyncRestFinished(result);
        }
    }

    private static class InsertAsyncTask extends AsyncTask<Food, Void, Void> {

        private FoodDAO asyncTaskDao;

        InsertAsyncTask(FoodDAO dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Food... params) {
            asyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class DeleteAsyncTask extends AsyncTask<Food, Void, Void> {

        private FoodDAO asyncTaskDao;

        DeleteAsyncTask(FoodDAO dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Food... params) {
            asyncTaskDao.delete(params[0]);
            return null;
        }
    }
}
