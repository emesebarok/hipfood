package com.example.assignment2;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.assignment2.ViewModel.FoodViewModel;
import com.example.assignment2.model.Food;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.sql.Ref;
import java.util.HashMap;
import java.util.Map;

public class OneFoodActivity extends AppCompatActivity {

    private static final String TAG = "OneFoodActivity";

    private int foodId = 0;
    private String foodName = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_food);
        Log.d(TAG, "onCreate: started.");

        Button addToCartButton = findViewById(R.id.add_to_cart);
        TextView textView = findViewById(R.id.food);


        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

        final FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference();
        final DatabaseReference fRef = ref.child(firebaseUser.getUid());
        Log.d(TAG, "userID: " + firebaseUser.getUid());
        final DatabaseReference newRef = fRef.push();

        getIncomingIntent();

        textView.setText(foodName);

        final FoodViewModel foodViewModel = ViewModelProviders.of(this).get(FoodViewModel.class);

        addToCartButton.findViewById(R.id.add_to_cart);
        addToCartButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                //add to cart
                Food food = foodViewModel.getFood(foodId);
                Log.d(TAG, "food:" + food.getId() + food.getName() + food.getMenuID());
                //newRef.setValue(food);
                //Map<String, Food> foodMap = new HashMap<>();
                //foodMap.put(String.valueOf(foodId), foodViewModel.getFood(foodId));
                //newRef.setValue(foodMap);
                newRef.setValue(foodId);
            }
        });
    }

    private void getIncomingIntent() {
        Log.d(TAG, "getIncomingIntent: checking for incoming intents.");

        if (getIntent().hasExtra("foodid") && getIntent().hasExtra("foodName")) {
            Log.d(TAG, "getIncomingIntent: found intent extras.");

            foodId = getIntent().getIntExtra("foodid", -1);
            foodName = getIntent().getStringExtra("foodName");
        }
    }
}
