package com.example.assignment2;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.example.assignment2.ViewModel.CustomerViewModel;
import com.example.assignment2.ViewModel.FoodViewModel;
import com.example.assignment2.ViewModel.MenuViewModel;
import com.example.assignment2.ViewModel.OrderDetailsViewModel;
import com.example.assignment2.ViewModel.OrderViewModel;
import com.example.assignment2.ViewModel.RestaurantViewModel;
import com.example.assignment2.model.Customer;
import com.example.assignment2.model.Food;
import com.example.assignment2.model.Menu;
import com.example.assignment2.model.Order;
import com.example.assignment2.model.OrderDetails;
import com.example.assignment2.model.Restaurant;

import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.List;

class ExportCSV {

    private final static String TAG = "ExportCSV";

    private CustomerViewModel customerViewModel;
    private FoodViewModel foodViewModel;
    private MenuViewModel menuViewModel;
    private OrderDetailsViewModel orderDetailsViewModel;
    private OrderViewModel orderViewModel;
    private RestaurantViewModel restaurantViewModel;
    private Context context;
    private StringBuilder c  = new StringBuilder();

    private List<Customer> customers;
    private List<Food> foods;
    private List<Menu> menus;
    private List<Order> orders;
    private List<OrderDetails> orderDetails;
    private List<Restaurant> restaurants;

    ExportCSV(Context context) {
        customerViewModel  = ViewModelProviders.of((FragmentActivity) context).get(CustomerViewModel.class);
        foodViewModel = ViewModelProviders.of((FragmentActivity) context).get(FoodViewModel.class);
        menuViewModel = ViewModelProviders.of((FragmentActivity) context).get(MenuViewModel.class);
        orderDetailsViewModel = ViewModelProviders.of((FragmentActivity) context).get(OrderDetailsViewModel.class);
        orderViewModel = ViewModelProviders.of((FragmentActivity) context).get(OrderViewModel.class);
        restaurantViewModel = ViewModelProviders.of((FragmentActivity) context).get(RestaurantViewModel.class);
        this.context = context;
    }

    void exportToFile(){
        Log.d(TAG, ": export started.");

        if(customers != null) {
            for (Customer i : customers) {
                c.append(i.getId()).append(", ").append(i.getName()).append(", ").append(i.getEmail()).append("\n");
            }
            Log.d(TAG, " customer done.");
        }

        if (foods != null) {
            for (Food i : foods) {
                c.append(i.getId()).append(", ").append(i.getName()).append(", ").append(i.getMenuID()).append("\n");
            }
            Log.d(TAG, " food done.");
        }

        if(menus != null) {
            for (Menu i : menus) {
                c.append(i.getId()).append(", ").append(i.getName()).append(", ").append(i.getRestID()).append("\n");
            }
            Log.d(TAG, " menu done.");
        }

        if (orderDetails != null) {
            for (OrderDetails i : orderDetails) {
                c.append(i.getId()).append(", ").append(i.getOrderID()).append(", ").append(i.getFoodID()).append("\n");
            }
            Log.d(TAG, " orderDetails done.");
        }

        if (orders != null) {
            for (Order i : orders) {
                c.append(i.getId()).append(", ").append(i.getCustomerID()).append(", ").append(i.getRestaurantID()).append("\n");
            }
            Log.d(TAG, " order done.");
        }

        if (restaurants != null) {
            for (Restaurant i : restaurants) {
                c.append(i.getId()).append(", ").append(i.getName()).append(", ").append("\n");
            }
            Log.d(TAG, " restaurant done.");
        }

        writeToFile(c.toString(), context);
    }

    private void writeToFile(String data, Context context) {
        //clearTheFile();
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("exported.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private void clearTheFile(){
        FileWriter fwOb = null;
        try {
            fwOb = new FileWriter("exported.txt", false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter pwOb = null;
        if (fwOb != null) {
            pwOb = new PrintWriter(fwOb, false);
        }
        if (pwOb != null) {
            pwOb.flush();
        }
        if (pwOb != null) {
            pwOb.close();
        }
        try {
            if (fwOb != null) {
                fwOb.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addToString(StringBuilder stringBuilder) {
        c.append(stringBuilder);
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public void setFoods(List<Food> foods) {
        this.foods = foods;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public void setOrderDetails(List<OrderDetails> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public void setRestaurants(List<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }
}
