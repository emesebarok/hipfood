package com.example.assignment2;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.assignment2.ViewModel.FoodViewModel;
import com.example.assignment2.adapters.FoodListAdapter;
import com.example.assignment2.model.Food;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class FoodActivity extends AppCompatActivity {
    private static final String TAG = "FoodActivity";
    private int menuId = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);
        Log.d(TAG, "onCreate: started.");

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                //finish();
                startActivity(new Intent(getApplicationContext(), CartActivity.class));
            }
        });

        Button button = findViewById(R.id.logout);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
                firebaseAuth.signOut();
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

        Button buttonOrders = findViewById(R.id.orders);
        buttonOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), OrderActivity.class));
            }
        });

        getIncomingIntent();

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final FoodListAdapter adapter = new FoodListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        FoodViewModel foodViewModel = ViewModelProviders.of(this).get(FoodViewModel.class);

        foodViewModel.getAllFoods(menuId).observe(this, new Observer<List<Food>>() {
            @Override
            public void onChanged(@Nullable final List<Food> foods) {
                Log.d(TAG, "onChanged: changed" + menuId);
                // Update the cached copy of the menus in the adapter.
                adapter.setFoods(foods);
            }
        });
    }

    private void getIncomingIntent() {
        Log.d(TAG, "getIncomingIntent: checking for incoming intents.");

        if (getIntent().hasExtra("menuid")) {
            Log.d(TAG, "getIncomingIntent: found intent extras.");

            menuId = getIntent().getIntExtra("menuid", -1);
        }
    }
}
