package com.example.assignment2.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.assignment2.MenuActivity;
import com.example.assignment2.R;
import com.example.assignment2.model.Restaurant;

import java.util.List;

public class RestaurantListAdapter extends RecyclerView.Adapter<RestaurantListAdapter.RestaurantViewHolder> {

    private static final String TAG = "RestaurantListAdapter";

    class RestaurantViewHolder extends RecyclerView.ViewHolder {
        private final TextView restaurantItemView;
        private final LinearLayout layout;

        private RestaurantViewHolder(View itemView) {
            super(itemView);
            restaurantItemView = itemView.findViewById(R.id.textView);
            layout = itemView.findViewById(R.id.recycler_view_item);
        }
    }

    private final LayoutInflater mInflater;
    private List<Restaurant> restaurants; // Cached copy of restaurants
    private Context context;

    public RestaurantListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @NonNull
    @Override
    public RestaurantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new RestaurantViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called");
        if (restaurants != null) {
            final Restaurant current = restaurants.get(position);
            holder.restaurantItemView.setText(current.getName());

            holder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG,"onClick: clicked on:" + restaurants.get(position));
                    //System.out.println(position);

                    Intent intent = new Intent(context, MenuActivity.class);
                    intent.putExtra("id", (position + 1));
                    context.startActivity(intent);

                }
            });

        } else {
            // Covers the case of data not being ready yet.
            holder.restaurantItemView.setText("No Restaurant");
        }
    }

    public void setRestaurants(List<Restaurant> restaurants){
        this.restaurants = restaurants;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // restaurants has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (restaurants != null)
            return restaurants.size();
        else return 0;
    }
}
