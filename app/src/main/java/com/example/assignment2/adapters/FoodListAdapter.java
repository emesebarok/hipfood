package com.example.assignment2.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.assignment2.OneFoodActivity;
import com.example.assignment2.R;
import com.example.assignment2.model.Food;

import java.util.List;

public class FoodListAdapter extends RecyclerView.Adapter<FoodListAdapter.FoodViewHolder> {

    private static final String TAG = "FoodListAdapter";

    class FoodViewHolder extends RecyclerView.ViewHolder {
        private final TextView foodItemView;
        private final LinearLayout layout;

        private FoodViewHolder(View itemView) {
            super(itemView);
            foodItemView = itemView.findViewById(R.id.textView);
            layout = itemView.findViewById(R.id.recycler_view_item);
        }
    }

    private final LayoutInflater mInflater;
    private List<Food> foods; // Cached copy of menus
    private Context context;

    public FoodListAdapter(Context context) {

        mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @NonNull
    @Override
    public FoodListAdapter.FoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new FoodListAdapter.FoodViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodListAdapter.FoodViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called");
        if (foods != null) {
            Food current = foods.get(position);
            holder.foodItemView.setText(current.getName());

            holder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "onClick: clicked on:" + foods.get(position));
                    System.out.println(position);

                    Intent intent = new Intent(context, OneFoodActivity.class);
                    intent.putExtra("foodid", foods.get(position).getId());
                    intent.putExtra("foodName", foods.get(position).getName());
                    context.startActivity(intent);
                }
            });
        }
        else {
            // Covers the case of data not being ready yet.
            holder.foodItemView.setText("No Menu");
        }
    }

    public void setFoods(List<Food> foods){
        this.foods = foods;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // menus has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (foods != null)
            return foods.size();
        else return 0;
    }
}
