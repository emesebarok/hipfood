package com.example.assignment2.adapters;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.assignment2.R;
import com.example.assignment2.ViewModel.FoodViewModel;
import com.example.assignment2.model.OrderDetails;

import java.util.List;

public class OrderFoodListAdapter extends RecyclerView.Adapter<OrderFoodListAdapter.OrderFoodViewHolder> {

    private static final String TAG = "OrderFoodListAdapter";

    class OrderFoodViewHolder extends RecyclerView.ViewHolder {
        private final TextView orderFoodItemView;
        private final LinearLayout layout;

        private OrderFoodViewHolder(View itemView) {
            super(itemView);
            orderFoodItemView = itemView.findViewById(R.id.textView);
            layout = itemView.findViewById(R.id.recycler_view_item);
        }
    }

    private final LayoutInflater mInflater;
    private List<OrderDetails> orderDetails; // Cached copy of orderdetails
    private FoodViewModel foodViewModel;

    public OrderFoodListAdapter(Context context) {

        mInflater = LayoutInflater.from(context);
        foodViewModel = ViewModelProviders.of((FragmentActivity) context).get(FoodViewModel.class);
    }

    @NonNull
    @Override
    public OrderFoodListAdapter.OrderFoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new OrderFoodListAdapter.OrderFoodViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderFoodListAdapter.OrderFoodViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called");
        if (orderDetails != null) {
            OrderDetails current = orderDetails.get(position);
            holder.orderFoodItemView.setText(foodViewModel.getFood(current.getFoodID()).getName());
        }
        else {
            // Covers the case of data not being ready yet.
            holder.orderFoodItemView.setText("No Order");
        }
    }

    public void setOrderDetails(List<OrderDetails> orderDetails){
        this.orderDetails = orderDetails;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // orders has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (orderDetails != null)
            return orderDetails.size();
        else return 0;
    }
}
