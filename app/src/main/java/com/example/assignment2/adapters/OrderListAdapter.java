package com.example.assignment2.adapters;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.assignment2.OrderFoodActivity;
import com.example.assignment2.R;
import com.example.assignment2.ViewModel.RestaurantViewModel;
import com.example.assignment2.model.Order;

import java.util.List;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.OrderViewHolder> {

    private static final String TAG = "FoodListAdapter";

    class OrderViewHolder extends RecyclerView.ViewHolder {
        private final TextView orderItemView;
        private final LinearLayout layout;

        private OrderViewHolder(View itemView) {
            super(itemView);
            orderItemView = itemView.findViewById(R.id.textView);
            layout = itemView.findViewById(R.id.recycler_view_item);
        }
    }

    private final LayoutInflater mInflater;
    private List<Order> orders; // Cached copy of orders
    private Context context;
    private RestaurantViewModel restaurantViewModel;

    public OrderListAdapter(Context context) {

        mInflater = LayoutInflater.from(context);
        this.context = context;
        restaurantViewModel = ViewModelProviders.of((FragmentActivity) context).get(RestaurantViewModel.class);
    }

    @NonNull
    @Override
    public OrderListAdapter.OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new OrderListAdapter.OrderViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderListAdapter.OrderViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called");
        if (orders != null) {
            Order current = orders.get(position);

            String t = current.getId() + " " + restaurantViewModel.getRestaurantById(current.getRestaurantID()).getName();
            holder.orderItemView.setText(t);

            holder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "onClick: clicked on:" + orders.get(position));
                    System.out.println(position);

                    Intent intent = new Intent(context, OrderFoodActivity.class);
                    intent.putExtra("orderid", orders.get(position).getId());
                    context.startActivity(intent);
                }
            });
        }
        else {
            // Covers the case of data not being ready yet.
            holder.orderItemView.setText("No Order");
        }
    }

    public void setOrders(List<Order> orders){
        this.orders = orders;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // orders has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (orders != null)
            return orders.size();
        else return 0;
    }
}
