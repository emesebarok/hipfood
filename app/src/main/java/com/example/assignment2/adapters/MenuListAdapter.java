package com.example.assignment2.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.assignment2.FoodActivity;
import com.example.assignment2.MenuActivity;
import com.example.assignment2.R;
import com.example.assignment2.model.Menu;

import java.util.List;

public class MenuListAdapter extends RecyclerView.Adapter<MenuListAdapter.MenuViewHolder> {

    private static final String TAG = "MenuListAdapter";

    class MenuViewHolder extends RecyclerView.ViewHolder {
        private final TextView menuItemView;
        private final LinearLayout layout;

        private MenuViewHolder(View itemView) {
            super(itemView);
            menuItemView = itemView.findViewById(R.id.textView);
            layout = itemView.findViewById(R.id.recycler_view_item);
        }
    }

    private final LayoutInflater mInflater;
    private List<Menu> menus; // Cached copy of menus
    private Context context;

    public MenuListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @NonNull
    @Override
    public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new MenuViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called");
        if (menus != null) {
            Menu current = menus.get(position);
            holder.menuItemView.setText(current.getName());

            holder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG,"onClick: clicked on:" + menus.get(position));
                    System.out.println(position);

                    Intent intent = new Intent(context, FoodActivity.class);
                    intent.putExtra("menuid", menus.get(position).getId());
                    context.startActivity(intent);

                }
            });
        } else {
            // Covers the case of data not being ready yet.
            holder.menuItemView.setText("No Menu");
        }
    }

    public void setMenus(List<Menu> menus){
        this.menus = menus;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // menus has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (menus != null)
            return menus.size();
        else return 0;
    }
}
