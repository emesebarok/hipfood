package com.example.assignment2;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.assignment2.ViewModel.CustomerViewModel;
import com.example.assignment2.ViewModel.FoodViewModel;
import com.example.assignment2.ViewModel.MenuViewModel;
import com.example.assignment2.ViewModel.OrderDetailsViewModel;
import com.example.assignment2.ViewModel.OrderViewModel;
import com.example.assignment2.ViewModel.RestaurantViewModel;
import com.example.assignment2.adapters.FoodListAdapter;
import com.example.assignment2.builder.BolognesePastaBuilder;
import com.example.assignment2.builder.Builder;
import com.example.assignment2.builder.CarbonaraPastaBuilder;
import com.example.assignment2.builder.CiorbaSoupBuilder;
import com.example.assignment2.builder.CreamSoupBuilder;
import com.example.assignment2.builder.FettuccineAlfredoPastaBuilder;
import com.example.assignment2.builder.HawaiianPizzaBuilder;
import com.example.assignment2.builder.SimplePizzaBuilder;
import com.example.assignment2.builder.SpicyPizzaBuilder;
import com.example.assignment2.builder.VegetableSoupBuilder;
import com.example.assignment2.model.Customer;
import com.example.assignment2.model.Food;
import com.example.assignment2.model.Menu;
import com.example.assignment2.model.Order;
import com.example.assignment2.model.OrderDetails;
import com.example.assignment2.model.Pasta;
import com.example.assignment2.model.Pizza;
import com.example.assignment2.model.Restaurant;
import com.example.assignment2.model.Soup;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class CartActivity extends AppCompatActivity {

    private static final String TAG = "CartActivity";
    private List<Integer> foodIdList = new ArrayList<>();
    private Context context = this;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        Log.d(TAG, "onCreate: started.");

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

        final FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference(firebaseUser.getUid());

        final FoodViewModel foodViewModel = ViewModelProviders.of(this).get(FoodViewModel.class);
        final OrderViewModel orderViewModel = ViewModelProviders.of(this).get(OrderViewModel.class);
        final OrderDetailsViewModel orderDetailsViewModel = ViewModelProviders.of(this).get(OrderDetailsViewModel.class);
        final CustomerViewModel customerViewModel = ViewModelProviders.of(this).get(CustomerViewModel.class);

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final FoodListAdapter adapter = new FoodListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dataSnapshot.getKey();
                foodIdListClean();
                //Log.d(TAG, "objects: " + Objects.requireNonNull(dataSnapshot.getValue()).toString());
                String f = "";
                try {
                    f = dataSnapshot.getValue().toString();

                    String[] words = f.split("=");
                    List<Integer> helper = new ArrayList<>();
                    for (String h:words) {
                        Log.d(TAG, h);
                        if (!h.startsWith("{")) {
                            int s=0, k=0;
                            while(Character.isDigit(h.charAt(k))) {
                                s = s*10+Character.getNumericValue(h.charAt(k));
                                k++;
                            }
                            addToFoodIdList(s);
                            helper.add(s);
                        }
                    }
                    List<Food> foods = new ArrayList<>();
                    for (Integer i:helper) {
                        Log.d(TAG, "i: "+ i);
                        Food food = foodViewModel.getFood(i);
                        Log.d(TAG, "food" + food);
                        foods.add(food);
                    }
                    adapter.setFoods(foods);
                } catch (NullPointerException npe) {
                    Log.d(TAG, " No food in cart.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        Button button = findViewById(R.id.finalize);
        button.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                //finalize order
                Builder builder = new Builder();
                BolognesePastaBuilder bolognesePastaBuilder = new BolognesePastaBuilder();
                CarbonaraPastaBuilder carbonaraPastaBuilder = new CarbonaraPastaBuilder();
                FettuccineAlfredoPastaBuilder fettuccineAlfredoPastaBuilder = new FettuccineAlfredoPastaBuilder();
                CiorbaSoupBuilder ciorbaSoupBuilder = new CiorbaSoupBuilder();
                CreamSoupBuilder creamSoupBuilder = new CreamSoupBuilder();
                VegetableSoupBuilder vegetableSoupBuilder = new VegetableSoupBuilder();
                HawaiianPizzaBuilder hawaiianPizzaBuilder = new HawaiianPizzaBuilder();
                SimplePizzaBuilder simplePizzaBuilder = new SimplePizzaBuilder();
                SpicyPizzaBuilder spicyPizzaBuilder = new SpicyPizzaBuilder();


                Order order = new Order();
                int orderId = orderViewModel.countOrdersForNextOrdersId() + 1;
                order.setId(orderId);
                order.setCustomerID(firebaseUser.getUid());
                order.setDelivered(false);
                order.setRestaurantID(foodViewModel.getRestId(foodIdList.get(0)));//getRestIdByFoodId
                orderViewModel.insert(order);
                for (Integer i:foodIdList) {
                    //make orderviewmodel
                    OrderDetails orderDetails = new OrderDetails();
                    orderDetails.setFoodID(i);
                    orderDetails.setId(orderDetailsViewModel.countOrdersForNextOrderDetailsId() + 1);
                    orderDetails.setOrderID(orderId);
                    orderDetailsViewModel.insert(orderDetails);
                    switch (i) {
                        case 1:
                            builder.setPastaBuilder(bolognesePastaBuilder);;
                            builder.constructPasta();
                            Pasta pasta = builder.getPasta();
                            break;
                        case 2:
                            builder.setPastaBuilder(carbonaraPastaBuilder);
                            builder.constructPasta();
                            Pasta pasta1 = builder.getPasta();
                            break;
                        case 3:
                            builder.setPastaBuilder(fettuccineAlfredoPastaBuilder);;
                            builder.constructPasta();;
                            Pasta pasta2 = builder.getPasta();
                            break;
                        case 4:
                            builder.setSoupBuilder(creamSoupBuilder);
                            builder.constructSoup();
                            Soup soup = builder.getSoup();
                            break;
                        case 5:
                            builder.setSoupBuilder(ciorbaSoupBuilder);
                            builder.constructSoup();
                            Soup soup1 = builder.getSoup();
                            break;
                        case 6:
                            builder.setSoupBuilder(vegetableSoupBuilder);
                            builder.constructSoup();
                            Soup soup2 = builder.getSoup();
                            break;
                        case 7:
                            builder.setPizzaBuilder(hawaiianPizzaBuilder);
                            builder.constructPizza();
                            Pizza pizza = builder.getPizza();
                            break;
                        case 8:
                            builder.setPizzaBuilder(simplePizzaBuilder);
                            builder.constructPizza();
                            Pizza pizza1 = builder.getPizza();
                            break;
                        case 9:
                            builder.setPizzaBuilder(spicyPizzaBuilder);
                            builder.constructPizza();
                            Pizza pizza2 = builder.getPizza();
                            break;
                        case 10:
                            builder.setPastaBuilder(bolognesePastaBuilder);
                            builder.constructPasta();
                            Pasta pasta3 = builder.getPasta();
                            break;
                        case 11:
                            builder.setPastaBuilder(carbonaraPastaBuilder);
                            builder.constructPasta();
                            Pasta pasta4 = builder.getPasta();
                            break;
                        case 12:
                            builder.setPastaBuilder(fettuccineAlfredoPastaBuilder);;
                            builder.constructPasta();;
                            Pasta pasta5 = builder.getPasta();
                            break;
                        case 14:
                            builder.setSoupBuilder(creamSoupBuilder);
                            builder.constructSoup();
                            Soup soup3 = builder.getSoup();
                            break;
                        case 13:
                            builder.setSoupBuilder(ciorbaSoupBuilder);
                            builder.constructSoup();
                            Soup soup4 = builder.getSoup();
                            break;
                        case 18:
                            builder.setPastaBuilder(bolognesePastaBuilder);;
                            builder.constructPasta();
                            Pasta pasta6 = builder.getPasta();
                            break;
                        case 19:
                            builder.setPastaBuilder(carbonaraPastaBuilder);
                            builder.constructPasta();
                            Pasta pasta7 = builder.getPasta();
                        case 20:
                            builder.setSoupBuilder(creamSoupBuilder);
                            builder.constructSoup();
                            Soup soup5 = builder.getSoup();
                            break;
                        case 22:
                            builder.setSoupBuilder(ciorbaSoupBuilder);
                            builder.constructSoup();
                            Soup soup6 = builder.getSoup();
                            break;
                        case 21:
                            builder.setSoupBuilder(vegetableSoupBuilder);
                            builder.constructSoup();
                            Soup soup7 = builder.getSoup();
                            break;
                        case 15:
                            builder.setPizzaBuilder(hawaiianPizzaBuilder);
                            builder.constructPizza();
                            Pizza pizza3 = builder.getPizza();
                            break;
                        case 17:
                            builder.setPizzaBuilder(simplePizzaBuilder);
                            builder.constructPizza();
                            Pizza pizza4 = builder.getPizza();
                            break;
                        case 16:
                            builder.setPizzaBuilder(spicyPizzaBuilder);
                            builder.constructPizza();
                            Pizza pizza5 = builder.getPizza();
                            break;
                    }
                }

                ref.removeValue();

//                ExportCSV exportCSV = new ExportCSV(context);
//                exportCSV.exportToFile();
                exportToFile();
            }
        });

    }

    void exportToFile(){

        final ExportCSV exportCSV = new ExportCSV(context);
        Log.d(TAG, " export started.");

        CustomerViewModel customerViewModel  = ViewModelProviders.of((FragmentActivity) context).get(CustomerViewModel.class);
        FoodViewModel foodViewModel = ViewModelProviders.of((FragmentActivity) context).get(FoodViewModel.class);
        MenuViewModel menuViewModel = ViewModelProviders.of((FragmentActivity) context).get(MenuViewModel.class);
        OrderDetailsViewModel orderDetailsViewModel = ViewModelProviders.of((FragmentActivity) context).get(OrderDetailsViewModel.class);
        OrderViewModel orderViewModel = ViewModelProviders.of((FragmentActivity) context).get(OrderViewModel.class);
        RestaurantViewModel restaurantViewModel = ViewModelProviders.of((FragmentActivity) context).get(RestaurantViewModel.class);
        final StringBuilder c  = new StringBuilder();

        customerViewModel.getCustomers().observe(this, new Observer<List<Customer>>() {
            @Override
            public void onChanged(@Nullable final List<Customer> customers) {
                Log.d(TAG, "onChanged: changed");
                // Update the cached copy of the words in the adapter.
                exportCSV.setCustomers(customers);
            }
        });

        foodViewModel.getFoods().observe(this, new Observer<List<Food>>() {
            @Override
            public void onChanged(@Nullable final List<Food> foods) {
                Log.d(TAG, "onChanged: changed");
                // Update the cached copy of the words in the adapter.
                exportCSV.setFoods(foods);
            }
        });

        menuViewModel.getMenus().observe(this, new Observer<List<Menu>>() {
            @Override
            public void onChanged(@Nullable final List<Menu> menus) {
                Log.d(TAG, "onChanged: changed");
                // Update the cached copy of the words in the adapter.
                exportCSV.setMenus(menus);
            }
        });

        orderDetailsViewModel.getOrderDetails().observe(this, new Observer<List<OrderDetails>>() {
            @Override
            public void onChanged(@Nullable final List<OrderDetails> orderDetails) {
                Log.d(TAG, "onChanged: changed");
                // Update the cached copy of the words in the adapter.
                exportCSV.setOrderDetails(orderDetails);
            }
        });

        orderViewModel.getOrders().observe(this, new Observer<List<Order>>() {
            @Override
            public void onChanged(@Nullable final List<Order> orders) {
                Log.d(TAG, "onChanged: changed");
                // Update the cached copy of the words in the adapter.
                exportCSV.setOrders(orders);
            }
        });

        restaurantViewModel.getRestaurants().observe(this, new Observer<List<Restaurant>>() {
            @Override
            public void onChanged(@Nullable final List<Restaurant> restaurants) {
                Log.d(TAG, "onChanged: changed");
                // Update the cached copy of the words in the adapter.
                exportCSV.setRestaurants(restaurants);
            }
        });

        exportCSV.exportToFile();
    }

    private void addToFoodIdList(int s) {
        foodIdList.add(s);
    }

    private void foodIdListClean(){
        foodIdList.clear();
    }
}
