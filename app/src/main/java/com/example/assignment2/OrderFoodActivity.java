package com.example.assignment2;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.assignment2.ViewModel.OrderDetailsViewModel;
import com.example.assignment2.adapters.OrderFoodListAdapter;
import com.example.assignment2.model.Order;
import com.example.assignment2.model.OrderDetails;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class OrderFoodActivity extends AppCompatActivity {
    private static final String TAG = "OrderFoodActivity";
    private int orderId = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderfood);
        Log.d(TAG, "onCreate: started.");

        Button button = findViewById(R.id.logout);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
                firebaseAuth.signOut();
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

        Button buttonOrders = findViewById(R.id.orders);
        buttonOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), OrderActivity.class));
            }
        });

        getIncomingIntent();

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final OrderFoodListAdapter adapter = new OrderFoodListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        OrderDetailsViewModel orderDetailsViewModel = ViewModelProviders.of(this).get(OrderDetailsViewModel.class);

        orderDetailsViewModel.getOrderDetailsByOrderId(orderId).observe(this, new Observer<List<OrderDetails>>() {
            @Override
            public void onChanged(@Nullable final List<OrderDetails> orderDetails) {
                Log.d(TAG, "onChanged: changed " + orderId);
                adapter.setOrderDetails(orderDetails);
            }
        });
    }

    private void getIncomingIntent() {
        Log.d(TAG, "getIncomingIntent: checking for incoming intents.");

        if (getIntent().hasExtra("orderid")) {
            Log.d(TAG, "getIncomingIntent: found intent extras.");

            orderId = getIntent().getIntExtra("orderid", -1);
        }
    }
}
