package com.example.assignment2.builder;

import com.example.assignment2.model.Soup;

public abstract class SoupBuilder {

    protected Soup soup;

    public Soup getSoup() {
        return soup;
    }

    public void createNewSoupProduct() {
        soup = new Soup();
    }

    public abstract void buildBase();
    public abstract void buildExtra();

}
