package com.example.assignment2.builder;

public class FettuccineAlfredoPastaBuilder extends PastaBuilder {
    @Override
    public void buildType() {
        pasta.setType("fettuccine");
    }

    @Override
    public void buildSauce() {
        pasta.setSauce("butter+parmesan cheese");
    }

    @Override
    public void buildExtra() {
        pasta.setExtra("prosciutto crudo");
    }
}
