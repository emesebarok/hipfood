package com.example.assignment2.builder;

public class BolognesePastaBuilder extends PastaBuilder {
    @Override
    public void buildType() {
        pasta.setType("tagliatelle");
    }

    @Override
    public void buildSauce() {
        pasta.setSauce("bolognese");
    }

    @Override
    public void buildExtra() {
        pasta.setExtra("meat balls");
    }
}
