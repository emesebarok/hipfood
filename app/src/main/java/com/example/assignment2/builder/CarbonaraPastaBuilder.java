package com.example.assignment2.builder;

public class CarbonaraPastaBuilder extends PastaBuilder {
    @Override
    public void buildType() {
        pasta.setType("spaghetti");
    }

    @Override
    public void buildSauce() {
        pasta.setSauce("cheese+pepper+eggs+guanciale");
    }

    @Override
    public void buildExtra() {
        pasta.setExtra("mushrooms+beans");
    }
}
