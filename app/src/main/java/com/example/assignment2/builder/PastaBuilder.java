package com.example.assignment2.builder;

import com.example.assignment2.model.Pasta;

public abstract class PastaBuilder {

    protected Pasta pasta;

    public Pasta getPasta() {
        return pasta;
    }

    public void createNewPastaProduct() {
        pasta = new Pasta();
    }

    public abstract void buildType();
    public abstract void buildSauce();
    public abstract void buildExtra();
}
