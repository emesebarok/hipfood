package com.example.assignment2.builder;

public class VegetableSoupBuilder extends SoupBuilder {
    @Override
    public void buildBase() {
        soup.setBase("vegetable stock");
    }

    @Override
    public void buildExtra() {
        soup.setExtra("none");
    }
}
