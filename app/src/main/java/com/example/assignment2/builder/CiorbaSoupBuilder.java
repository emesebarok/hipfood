package com.example.assignment2.builder;

public class CiorbaSoupBuilder extends SoupBuilder {
    @Override
    public void buildBase() {
        soup.setBase("chicken stock");
    }

    @Override
    public void buildExtra() {
        soup.setExtra("meat balls");
    }
}
