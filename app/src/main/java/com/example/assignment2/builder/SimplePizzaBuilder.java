package com.example.assignment2.builder;

public class SimplePizzaBuilder extends PizzaBuilder {

    public void buildDough() {
        pizza.setDough("cross");
    }

    public void buildSauce() {
        pizza.setSauce("ketchup");
    }

    public void buildTopping() {
        pizza.setTopping("ham+olive");
    }

}
