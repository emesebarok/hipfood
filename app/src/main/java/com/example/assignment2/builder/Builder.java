package com.example.assignment2.builder;

import com.example.assignment2.model.Pasta;
import com.example.assignment2.model.Pizza;
import com.example.assignment2.model.Soup;

public class Builder {

    private PizzaBuilder pizzaBuilder;
    private PastaBuilder pastaBuilder;
    private SoupBuilder soupBuilder;

    public void setPizzaBuilder(PizzaBuilder pb) {
        pizzaBuilder = pb;
    }

    public void setPastaBuilder(PastaBuilder pastaBuilder) {
        this.pastaBuilder = pastaBuilder;
    }

    public void setSoupBuilder(SoupBuilder soupBuilder) {
        this.soupBuilder = soupBuilder;
    }

    public Pizza getPizza() {
        return pizzaBuilder.getPizza();
    }

    public Pasta getPasta() {
        return pastaBuilder.getPasta();
    }

    public Soup getSoup() {
        return soupBuilder.getSoup();
    }

    public void constructPizza() {
        pizzaBuilder.createNewPizzaProduct();
        pizzaBuilder.buildDough();
        pizzaBuilder.buildSauce();
        pizzaBuilder.buildTopping();
    }

    public void constructSoup() {
        soupBuilder.createNewSoupProduct();
        soupBuilder.buildBase();
        soupBuilder.buildExtra();
    }

    public void constructPasta() {
        pastaBuilder.createNewPastaProduct();
        pastaBuilder.buildType();
        pastaBuilder.buildSauce();
        pastaBuilder.buildExtra();
    }

}
