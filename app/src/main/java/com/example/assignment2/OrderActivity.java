package com.example.assignment2;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.example.assignment2.ViewModel.OrderViewModel;
import com.example.assignment2.adapters.OrderListAdapter;
import com.example.assignment2.model.Order;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class OrderActivity extends AppCompatActivity {

    private static final String TAG = "OrderActivity";
    private int orderId = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        Log.d(TAG, "onCreate: started.");

        Button button = findViewById(R.id.logout);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
                firebaseAuth.signOut();
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

        Button buttonOrders = findViewById(R.id.orders);
        buttonOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final OrderListAdapter adapter = new OrderListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        OrderViewModel orderViewModel = ViewModelProviders.of(this).get(OrderViewModel.class);
        final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

        orderViewModel.findOrdersByCustomerId(firebaseAuth.getUid()).observe(this, new Observer<List<Order>>() {
            @Override
            public void onChanged(@Nullable final List<Order> orders) {
                Log.d(TAG, "onChanged: changed " + firebaseAuth.getUid());
                // Update the cached copy of the menus in the adapter.
                adapter.setOrders(orders);
            }
        });
    }
}
