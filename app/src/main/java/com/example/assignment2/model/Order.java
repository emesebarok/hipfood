package com.example.assignment2.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.List;

@Entity(foreignKeys = {
        @ForeignKey(entity = Customer.class,
                parentColumns = "id",
                childColumns = "cust_id"),

        @ForeignKey(entity = Restaurant.class,
                parentColumns = "id",
                childColumns = "rest_id")},
        indices = {
        @Index(value = "cust_id"),
        @Index(value = "rest_id")})
public class Order {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name="cust_id")
    private String customerID;

    @ColumnInfo(name="rest_id")
    private int restaurantID;

    private boolean delivered;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public int getRestaurantID() {
        return restaurantID;
    }

    public void setRestaurantID(int restaurantID) {
        this.restaurantID = restaurantID;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }
}
