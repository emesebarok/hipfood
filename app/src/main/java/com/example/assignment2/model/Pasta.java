package com.example.assignment2.model;

public class Pasta {

    private String type = "";
    private String sauce = "";
    private String extra = "";

    public void setType(String type) {
        this.type = type;
    }

    public void setSauce(String sauce) {
        this.sauce = sauce;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

}
