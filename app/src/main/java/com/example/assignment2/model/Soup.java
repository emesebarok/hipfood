package com.example.assignment2.model;

public class Soup {

    private String base = "";
    private String extra = "";

    public void setBase(String base) {
        this.base = base;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }
}
