package com.example.assignment2.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(foreignKeys = {
        @ForeignKey(entity = Order.class,
                parentColumns = "id",
                childColumns = "order_id"),

        @ForeignKey(entity = Food.class,
                parentColumns = "id",
                childColumns = "food_id")},
        indices = {
        @Index(value = "order_id"),
        @Index(value = "food_id")
        })
public class OrderDetails {

    @PrimaryKey
    @NonNull
    private int id;

    @ColumnInfo(name="order_id")
    private int orderID;

    @ColumnInfo(name="food_id")
    private int foodID;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public int getFoodID() {
        return foodID;
    }

    public void setFoodID(int foodID) {
        this.foodID = foodID;
    }
}
