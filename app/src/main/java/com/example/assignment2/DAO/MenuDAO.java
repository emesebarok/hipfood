package com.example.assignment2.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.example.assignment2.model.Menu;

import java.util.List;

@Dao
public interface MenuDAO extends BaseDAO<Menu> {
    @Query("select * from menu")
    LiveData<List<Menu>> loadAllMenus();

    @Query("select * from menu where rest_id = :id")
    LiveData<List<Menu>> loadAllMenusByRestId(int id);

    @Query("select * from menu where id = :id")
    Menu loadMenuById(int id);

    @Query("DELETE FROM menu")
    void deleteAll();
}
