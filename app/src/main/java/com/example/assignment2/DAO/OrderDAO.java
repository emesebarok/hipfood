package com.example.assignment2.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.example.assignment2.model.Food;
import com.example.assignment2.model.Order;

import java.util.List;

@Dao
public interface OrderDAO extends BaseDAO<Order> {
    @Query("select * from 'order'")
    LiveData<List<Order>> loadAllOrders();

    @Query("select * from 'order' where rest_id = :id")
    LiveData<List<Order>> loadAllOrdersByRestId(int id);

    @Query("select * from 'order' where delivered = 1 and rest_id = :id")
    LiveData<List<Order>> loadDeliveredOrders(int id);

    @Query("select * from 'order' where delivered = 0 and rest_id = :id")
    LiveData<List<Order>> loadNotDeliveredOrders(int id);

    @Query("select * from 'order' where cust_id = :id")
    LiveData<List<Order>> loadOrdersByCustId(String id);

    @Query("select food.id, food.menu_id, food.name from food inner join OrderDetails on OrderDetails.order_id = food.id where OrderDetails.order_id = :id")
    LiveData<List<Food>> loadFoodsByOrderId(int id);

    @Query("DELETE FROM `order`")
    void deleteAll();
}
