package com.example.assignment2.DAO;

import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Update;

public interface BaseDAO<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(T object);

    @Delete
    void delete(T object);

    @Update
    void update(T object);
}
