package com.example.assignment2.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.example.assignment2.model.OrderDetails;

import java.util.List;

@Dao
public interface OrderDetailsDAO extends BaseDAO<OrderDetails> {
    @Query("select * from OrderDetails")
    LiveData<List<OrderDetails>> loadAllOrderDetails();

    @Query("select * from OrderDetails where order_id = :id")
    LiveData<List<OrderDetails>> loadOrderDetailsById(int id);

    @Query("DELETE FROM OrderDetails")
    void deleteAll();
}
