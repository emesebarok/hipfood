package com.example.assignment2.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import com.example.assignment2.model.Food;

import java.util.List;

@Dao
public interface FoodDAO extends BaseDAO<Food> {
    @Query("select * from food")
    LiveData<List<Food>> loadAllFoods();

    @Query("select * from food where menu_id = :id")
    LiveData<List<Food>> loadAllFoodsByMenuId(int id);

    @Query("select * from food where id = :id")
    Food loadFoodById(int id);

    @Query("DELETE FROM food")
    void deleteAll();

    @Query("select menu.rest_id from menu inner join food on food.menu_id = menu.id where food.id = :foodId")
    int returnRestId(int foodId);
}
