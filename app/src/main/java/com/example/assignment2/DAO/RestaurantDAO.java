package com.example.assignment2.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import com.example.assignment2.model.Restaurant;

import java.util.List;

@Dao
public interface RestaurantDAO extends BaseDAO<Restaurant> {
    @Query("select * from restaurant")
    LiveData<List<Restaurant>> loadAllRestaurants();

    @Query("select * from restaurant where id = :id")
    Restaurant loadRestaurantById(int id);

    @Query("DELETE FROM restaurant")
    void deleteAll();
}
