package com.example.assignment2.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.example.assignment2.model.Customer;

import java.util.List;

@Dao
public interface CustomerDAO extends BaseDAO<Customer> {

    @Query("select * from customer")
    LiveData<List<Customer>> allCustomers();

    @Query("select * from customer where email = :email")
    Customer findCustomerByEmail(String email);

    @Query("DELETE FROM customer")
    void deleteAll();
}
